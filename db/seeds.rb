# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

def pre_load(type)
	print "Gerando os #{type}"
end

def show_load()
	print " ."
	sleep(0.3)
	print "."
	sleep(0.3)
	print "."
	sleep(0.3)
	puts " OK!"
	sleep(0.3)
end

Kind.create!([
	{description: "Amigo"},
	{description: "Contato"},
	{description: "Comercial"}
])
pre_load('tipos de contatos (Kinds)')
show_load

