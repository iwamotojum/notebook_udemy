Rails.application.routes.draw do

  get '/bemvindo' => 'home#index'
  root 'home#index'

  resources :phones
  resources :addresses #only: [:new, show]
  resources :contacts #excepy: [:edit]
  resources :kinds

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

end
