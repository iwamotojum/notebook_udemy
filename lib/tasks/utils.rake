namespace :utils do

  desc "Popular banco de dados."
  task seed: :environment do
  
  	pre_load('contatos (Contacts)')
  	100.times do |i|
			Contact.create!(
			name: Faker::Name.name,
			email: Faker::Internet.email,
			kind: Kind.all.sample,
			rmk: LeroleroGenerator.sentence((1..3).to_a.sample)
			)
		end
		show_load


		pre_load('endereços (Addresses)')
		Contact.all.each do |contact|
			Address.create!(
				street: Faker::Address.street_name,
				city: Faker::Address.city,
				state: Faker::Address.state_abbr,
				contact: contact
				)
		end
		show_load

		pre_load('telefones (Phones)')
		Contact.all.each do |contact|
			Random.rand(1..5).times do |i|
				Phone.create!(
					phone: Faker::PhoneNumber.cell_phone ,
					contact: contact
					)
				end
			end
		show_load

	end

end
